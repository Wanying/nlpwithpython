'''
This module is used to convert a traditional Chinese symbol to a simplified Chinese symbol
@author: Wanying Ding
@email: wd78@drexel.edu
@date: 2014/11/20
'''
def getDictionary():
    tradition_simple_dict=dict()
    file=open('C://Project//PythonLearning//data//chinese_dictionary.txt','r',encoding='UTF-8')
    lines=file.readlines()
    for line in lines:
        terms=line.split(' ')
        for term in terms:
            term =term.strip()
            if term=='':
                continue
            words=term.split('(')
            simple=words[0].strip()
            tradition = words[1].replace(')','').strip()
           # print("SIMPLE: {}, TRADITION: {}".format(simple,tradition))
            tradition_simple_dict[tradition]=simple
    return tradition_simple_dict

