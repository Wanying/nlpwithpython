'''
@author Wanying
@email wd78@drexel.edu
@date 2014/11/19
'''

import sys
import mysql.connector
import json

user = 'root'
pwd = '1234'
host = '127.0.0.1'
db = 'douban_movie'

select_sql = 'select * from movielist where date>=2000'

cox = mysql.connector.connect(user=user, password=pwd, host=host, database=db)
cursor = cox.cursor()
pcursor = cox.cursor()

file = open('C://Project//MoviePredict//movie_data//movieData.txt', 'w', encoding='UTF-8')
# define a function to process the person data
def getPersonList(personStr, personCursor):
    if personStr.strip() == '':
        return
    else:
        pList = list()
        personlist = personStr.split(',')
        for person in personlist:
            select_sql = "select id,name,followers from personlist where id=" + person
            try:
                personCursor.execute(select_sql)
                results = personCursor.fetchall()
                for person in results:
                    id = person[0]
                    name = person[1]
                    followers = person[2]
                    data = {'id': id, 'name': name, 'followers': followers}
                    pList.append(data)
            except mysql.connector.Error as err1:
                print("Errors in processing person data")
                print('Error: {}'.format(err1.msg))
        return pList


try:
    movieList = list()
    cursor.execute(select_sql)
    results = cursor.fetchall()
    for row in results:

        mid = row[0]
        name = row[1]
        actors = row[2]
        directors = row[3]
        editors = row[4]
        type = row[5]
        date = row[6]
        country = row[7]
        description = row[8]
        isMovie = row[14]
        tags = row[15]

        #Only process movie information
        if isMovie == 1:
            movieList = list()
            #print("ID: {}, Name: {}".format(mid,name))
            #process actors information
            actorList = getPersonList(actors, pcursor)
            #process editors information
            editorList = getPersonList(editors, pcursor)
            #process directors information
            directorList = getPersonList(directors, pcursor)

            movie = {'movieID': mid, 'movieName': name, 'actors': actorList, 'editors': editorList,
                     'directors': directorList, 'type': type, 'date': date, 'country': country,
                     'description': description, 'tags': tags}
            print(movie)
            json.dump(movie,file,ensure_ascii=False)
            file.write('\n')
    file.flush()
    file.close()


except mysql.connector.Error as err:
    print('select data fails.')
    print('Error: {}'.format(err.msg))
    sys.exit()



