
"""
@author Wanying
@email wd78@drexel.edu
@date: 2014/11/19
"""


import mysql.connector
import re
import string
import moviePredict.t2sch as t2sch

user ='root'
pwd='1234'
host='127.0.0.1'
db='douban_movie'

#Convert country information

cox = mysql.connector.connect(user=user,password=pwd,host=host,database=db)
curser=cox.cursor()
t2schDict = t2sch.getDictionary()
punc = set(string.punctuation)

def makeCountryDictionary():
    country_select="select country from movielist where is_movie=1 and date>=2000"
    country_dict = dict()
    cid = 0
    try:
        curser.execute(country_select)
        results = curser.fetchall()
        for r in results:
            cstr = r[0].strip()
            if cstr == '':
                continue
            countries=cstr.split('/')
            for country in countries:
                country=re.sub(r"[a-zA-Z]","",country)
                country=country.strip()
                if country == '':
                    continue
                c=0
                newCountry=''
                while c<country.__len__():
                    word=country[c]
                    c+=1
                    if word in t2schDict:
                        word = t2schDict[word]
                        newCountry+=word
                    elif word in punc:
                        continue
                    else:
                        newCountry+=word

                if newCountry=='香港':
                    newCountry='中国香港'
                if newCountry=='台湾':
                    newCountry='中国台湾'
                if newCountry=='中国':
                    newCountry='中国大陆'

                if newCountry in country_dict:
                    continue
                elif newCountry=='':
                    continue
                else:
                    country_dict[newCountry]=cid
                    cid += 1
        print(country_dict)
        return country_dict
    except mysql.connector.Error as err:
        print(err.msg)
        return

def convertCountry(countryStr):
    countryDict = makeCountryDictionary()
    countryArray=[]
    i=0
    while i<countryDict.__len__():
        countryArray.append(0)
        i+=1
    countries=countryStr.split('/')
    for country in countries:
        country=re.sub(r"[a-zA-Z]","",country)
        country=country.strip()
        if(country==''):
            continue
        c=0
        newCountry=''
        while c<country.__len__():
            word=country[c]
            c+=1
            if word in t2schDict:
                word=t2schDict[word]
                newCountry+=word
            elif word in punc:
                continue
            else:
                newCountry+=word
        if newCountry=='香港':
            newCountry='中国香港'
        if newCountry=='台湾':
            newCountry='中国台湾'
        if newCountry=='中国':
            newCountry='中国大陆'

        cid=countryDict[newCountry]
        countryArray[cid]=1
    return countryArray

def makeTypeDictionary():
    type_select="select type from movielist where is_movie=1 and date>=2000"
    type_dict = dict()
    tid=0
    try:
        curser.execute(type_select)
        results=curser.fetchall()
        for result in results:
            typestr=result[0].strip()
            typestr=re.sub(r"[a-zA-Z-]","",typestr)
            if typestr=='':
                continue
            types=typestr.split('/')
            for type in types:
                type=type.strip()
                if type=='':
                    continue
                if type in type_dict:
                    continue
                else:
                    type_dict[type]=tid
                    tid+=1
        return type_dict
    except mysql.connector.Error as err:
        print(err.msg)
        return

def convertType(typeStr):
    typeDict=makeTypeDictionary()
    typeArray=[]
    i=0

    while i<typeDict.__len__():
        typeArray.append(0)
        i+=1

    types=typeStr.split('/')
    for type in types:
        if type=='':
            continue
        type=re.sub(r"[a-zA-Z-]","",type)
        if type=='':
            continue
        tid = typeDict[type]
        typeArray[tid]=1
    print(typeArray)
    return typeArray

def convertDate(dateStr):
    dateArray=[]
    i=0
    while i<4:
        dateArray.append(0)
        i+=1

convertType("喜剧/动作/惊悚/犯罪/")


