__author__ = 'TCL'
#List might contain items of different types, but usually the items all have the same type
squares=[1,4,9,16,25]
print(squares)
#Like String, lists can be indexed and sliced
print(squares[0])
print(squares[:2])
#List also supports operations like concatenation
newsquares=squares+[36,49,64,81,100]
print(newsquares)
#Unlike strings, which are immutable, lists are a mutable type, it is possible to change their content
cubes=[1,8,27,65,125]
cubes[3]=64
print(cubes)
#You can also add new items at the end of the list, by using the append() method
cubes.append(216)
cubes.append(7**3)
print(cubes)
#Assignment to slices is also possible, and this can even change the size of the list or clear it entirely
letters=['a','b','c','d','e','f','g']
print(letters)
letters[2:5]=['C','D','E']
print(letters)
letters[2:5]=[]
print(letters)
print(len(letters))
# It is possible to nest lists, for example
a=['a','b','c']
n=[1,2,3]
x=[a,n]
print(x)
