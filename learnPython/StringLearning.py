__author__ = 'TCL'


str1 = 'spam eggs' #single quotes
str2 = "spam eggs" # double quotes
str3 = 'doesn\'t' # use \' to escape the single quote
print(str3)

# String can be concatenated with the + operator, and repeated with *
str4  = 3*'un'+'ium'
print(str4)

#two or more string literal next to each other are automatically concatenated

"""String can be indexed, with the first character having index 0.
There is no separate character type; a character is a string of size one
"""

word='Python'
print(word[0]+" "+word[5])

"""
Note how the start is always included, and the end always excluded. THis makes sure that s[:i]+s[i:] is always
equals to s
"""
print(word[:2])
print(word[2:])
print(word[:2]+word[2:])