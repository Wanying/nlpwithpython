__author__ = 'TCL'

words=['cat','window','defenestrate']
for w in words:
    print(w,len(w))


for w in words[:]: #Loop over a slice copy of the entire list
    if(len(w)>6):
        words.insert(0,w)
print(words)

#The range() function
#If you do need to iterate over a sequence of numbers, the built in function range() comes in handy
for i in range(5):
    print(i)


a=["Mary","had","a","little","lamb"]
for i in range(len(a)):
    print(i,a[i])